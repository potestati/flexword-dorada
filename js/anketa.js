function posaljiOdgovor(brojPitanja, odgovor) {
    //ta funkcija se koristi i za zapocinjanje ankete kada nemamo odgovor dok jos nismo odgovorili ni na jedno pitanje
    //kada je argument broj pitanje jednako 0 nula

    var ajax_post_url = "api.php"; //adresa apija koji odgovara na nas poziv
    //pripremamo promenljive koje cemo poslati post metodom ajax sucess
    var data_plainobject = {
        pitanje: brojPitanja,
        odgovor: odgovor
    };
    $.post(ajax_post_url, data_plainobject, function (data) {
        // AJAX SUCCESS
        // A callback function that is executed if the request succeeds
        console.log("ajax success");
        console.log(data);
        window.broj_pitanja++; //uvecavamo broj za sledece pitanje

        if (data.status == "ok") {
            //case (predvidjen slucaj) kada postoji sledece pitanje
            prikaziPitanje(data); 
        } else if (data.status == "finished") {
            //case kada smo odgovorili na poslednje pitanje i nema sledeceg pitanja
            prikaziKrajAnkete();
        } else if (data.error) {
            //case kada korisniku nije dozvoljeno da ucestvuje u anketi
            prikaziGresku(data);
        }

    });
}

function prikaziPitanje(data) {
    var poljeZaOdgovor = "<br><input type='number' placeholder='0' name='odgovor' min=1 max=5>";
    //moze se gornje polje drugacije dizajnirati, cilj je da odgovor bude od jedan do 5 kako bi mogao graficki da se ispise i statisticki sabira
    //ako je druga forma treba da pri eventu slanja forme se na odgovarajuci nacin iscita
    var html = data.next_question.questionID + " pitanje: " + " " + data.next_question.text_pitanja + poljeZaOdgovor;
    $("#anketa").html(html)
}
function prikaziKrajAnkete() {
    var html = "nema vise pitanja anketa je zavrsena hvala";
    $("#anketa").html(html)
}
function prikaziGresku(data) {
    var html = "Greska! " + data.error_message;
    $("#anketa").html(html)
}


$(document).ready(function () {
    // izvrsava se kada se stranica ucita do kraja
    $("#anketa").html("izmenjeno"); //testiramo da li je zapocelo izvrsenje skripta. ako ova rec ostane na ekranu
    //znaci da je script zapocet ali u medjuvremenu srusen
    window.broj_pitanja = 0; //api ce za taj slucaj da zna da je to pocetak ankete
    //TODO globalnu promenljivu window.broj_pitanja treba kasnije zameniti metodom objekta , ceo skript treba da bude jedan js objekt
    posaljiOdgovor(window.broj_pitanja);//to je prvobitno slanje zahteva za prvo pitanje cim se skript pokrene




    $("#posalji").click(function () {
        //event slanje forme
        //saljem odgovor na klik korisnika
        var odgovor = $('input[name="odgovor"]').val(); //iscitavamo odgovor koji je korisnik uneo 
        console.log(odgovor);
        // TODo treba i validacija forme da proveri da li su obavezna polja popunjana ili da postoji default vrednost
        posaljiOdgovor(window.broj_pitanja, odgovor);
    });

});


