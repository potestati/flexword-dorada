<?php

//TODO
/*
 * ceo skript treba da bude jedna klasa a sve funkcije metode jedne klase
 * globalne promenljive treba zameniti metodama neke klase
 * 
 *  */

// global variables
$myIP = $_SERVER['REMOTE_ADDR'];
$myIP_hash = sha1($myIP);
$pre_1god_e = time() - ( 365 * 24 * 60 * 60 );
$trenutno_vreme = time();

// Konekcija sa bazom
define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_IME", "anketa");

$db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_IME);
if (mysqli_connect_errno()) {
    die("Konekcija nije uspela: " .
            mysqli_connect_error() .
            " (" . mysqli_connect_errno() . ")"
    );
}

// API
// nekisajt.com/api.php?ppitanje=1&odgovor=345
// FAZA upisa ankete u bazu

function userImaDozvolu($myIP_hash) {
    global $db;
    $myIP_hash_e = mysqli_real_escape_string($db, $myIP_hash);
    global $pre_1god_e;

    $query = mysqli_query($db, "SELECT * FROM users WHERE `hash_ip` = '$myIP_hash_e' AND `last_visit` > $pre_1god_e LIMIT 1");
    $arr = mysqli_fetch_assoc($query);


    if (count($arr) > 0) {
        // vec postoji, prikazi gresku da ne moze da glasa
        // NEMA DOZVOLU ZNACI FALSE
        return FALSE;

        //return TRUE;// da ne gnjavi
    } else {
        // ne postoji, upisi glasanje i ip adresu u tabelu
        // IMA DOZVOLU ZNACI TRUE
        return TRUE;
    }
}

function receive_answer() {

    global $db;
    global $trenutno_vreme;

    global $myIP;
    global $myIP_hash;
    $pitanjeID = $_POST['pitanje'];

    if (!userImaDozvolu($myIP_hash) && $pitanjeID == 0) {
        // provera da li je on vec ucestvovao u anketi SAMO ZA POCETAK ANKETE OBAVLJA OVU PROIVERU
        response_error(); // prekid skripta greskom ako nema dozvolu 
    } else {
        //ako korisnik nije ucestvovao u anketi upisi ga u bazu da ne bi mogao jos jednom 
        $query = mysqli_query($db, "INSERT INTO `users` (`hash_ip`, `last_visit`) VALUES ('$myIP_hash',  '$trenutno_vreme')");
    }

    if (!empty($_POST['odgovor'])) {
//TODO treba kasnije da se uradi da u slucaju kada je pitanje id = 0 da preskocimo upis u bazu
        //posto se pod pitanjem 0 podrazumeva da je pitanje 0 pocetak skripte

        $odgovor = $_POST['odgovor'];
        // TREBA da se proveri ako nema odgovor. to je za slucaj kada frontend trzi prvo pitanje
        // ako ne primi nikakve argumente da vrati pitanje broj 1
        // escaped vrednosti, radi sigurnosti od sql injection, za upis u bazu

        $pitanjeID_e = mysqli_real_escape_string($db, $pitanjeID);
        $odgovor_e = mysqli_real_escape_string($db, $odgovor);
        // upis u bazu odgovora na pitanje ....

        $query = mysqli_query($db, "INSERT INTO `odgovori` (`questionID`, `odgovor`) VALUES ('$pitanjeID_e',  '$odgovor_e')");
    }

    return response_nextquestion($pitanjeID);
}

function response_error() {
    $json = json_encode(array(
        'error' => true,
        'error_message' => 'Sa ovog računara je već učestvovano u anketi. '
    ));
    api_response_json($json);
    exit;
}

function get_question($questionID) {
    global $db;
    $questionID_e = mysqli_real_escape_string($db, $questionID);
    $query = mysqli_query($db, "SELECT * FROM pitanja WHERE `questionID` = '$questionID_e' LIMIT 1");
    $question = mysqli_fetch_assoc($query);
    return $question;
}

function response_nextquestion($questionID) {
    $next_question = get_question($questionID + 1);
    if (count($next_question) < 1) {
        return response_finish();
    }
    $json = json_encode(array(
        'status' => 'ok',
        'next_question' => $next_question
    ));
    api_response_json($json);
    exit;
}

function response_finish() {
    $json = json_encode(array(
        'status' => 'finished',
        'error' => false
    ));
    api_response_json($json);
    exit;
}

function api_response_json($json) {
    // return json data to frontend
    header('Content-Type: application/json');
    echo $json;
    exit;
}

// START skript
receive_answer();
