<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="flexword">
        <meta name="keywords" content="flexword">
        <meta name="author" content="flexword">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>flexword</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet">        
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <header>
            <p class="col-md-4">Flexword Translations &#38; Consultation</p>
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <p class="responsive-undu">
                <i class="fa fa-undo" aria-hidden="true"></i>
                START FORM AGAIN
            </p>
            <div class="dropdown">
                <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-globe" aria-hidden="true"></i> Language
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <li>
                        <div>
                            <span>
                                <p>Select language</p>
                                <input class="color" value="english" name="language" type="radio">
                                <label>English</label>
                            </span>
                            <span>
                                <input class="color" value="deutsch" name="language" type="radio">
                                <label>Deutsch</label>
                            </span>
                        </div> 
                    </li>
                </ul>
            </div>
            <p class="undu">
                <i class="fa fa-undo" aria-hidden="true"></i>
                START FORM AGAIN
            </p>
        </header>
        <div class="content">
            <div id="anketa">
                prvobitno
            </div>
            <div id="posalji">
                POSALJI
            </div>
            <div class="container">
                <div class="row">
                    <h1>YOUR OPINION IS IMPORTANT TO US</h1>
                    <p>
                        We at flexword strongly beleve that a good relationship 
                        with our translators is just as important as customer satisfaction. 
                        Please help us become a more reliable partner &#8208; the questionnaire 
                        will take no more than 2 minutes &#58;
                    </p>
                    <div class="question">
                        <h3>QUESTION 1 TEXT</h3>
                        <ul>
                            <li>
                                <i class="fa fa-star click" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <p>Mostly satisfied</p>
                        </ul>
                    </div>
                    <div class="question">
                        <h3>QUESTION 1 TEXT</h3>
                        <ul>
                            <li>
                                <i class="fa fa-star click" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <p>Just the right amount</p>
                        </ul>
                    </div>
                    <div class="question">
                        <h3>QUESTION 1 TEXT</h3>
                        <ul>
                            <li>
                                <i class="fa fa-star click" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <li>
                                <i class="fa fa-star" aria-hidden="true"></i> 
                            </li>
                            <p>More and less the same as other LSPs </p>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="logo">
            <div class="container">
                <a href="index.php">
                    <img src="img/logo.png">
                </a>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/anketa.js"></script>
    <!-- Main Script -->
    <script src="js/main.js" type="text/javascript"></script>


